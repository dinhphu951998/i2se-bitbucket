/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_manager;

/**
 *
 * @author Dante nguyen
 */
public class book {
    int code;
    String title;

    public book() {
    }

    public book(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setTitle(String title) {
        
        this.title = title;
    }

    @Override
    public String toString() {
        return "book{" + "code=" + code + ", title=" + title + '}';
    }
    
}
