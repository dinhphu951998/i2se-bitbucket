/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_manager;

/**
 *
 * @author Dante nguyen
 */
public class scienceBook extends book{
    String type;

    public scienceBook() {
    }

    public scienceBook(String type) {
        this.type = type;
    }

    public scienceBook(int code, String title,String type) {
        super(code, title);
        this.type=type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString()+" type="+type; //To change body of generated methods, choose Tools | Templates.
    }
    
}
