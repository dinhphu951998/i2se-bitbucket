/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_manager;

import java.awt.RenderingHints;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Dante nguyen
 */
public class BookTest {

    public static void main(String[] args) {
        int choice;
        bookList x = new bookList();
        Scanner sc = new Scanner(System.in);
        int realcode = 0;
        do {
            System.out.println("1.	Add a Detective Book");
            System.out.println("2.	Add a ScienceBook");
            System.out.println("3.	Delete a book");
            System.out.println("4.	Update a book");
            System.out.println("5.	Display All DetectiveBooks");
            System.out.println("6.	Display All ScienceBooks");
            System.out.println("7.	Exit");
            System.out.print("Your choice: ");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    while (true) {
                        System.out.print("Nhap code:");
                        sc = new Scanner(System.in);
                        String code = sc.nextLine();
                        realcode = Check.nhapInteger(code);
                        if (realcode > 0) {
                            break;
                        } else {
                            System.out.println("Code must be integer");
                        }
                    }
                    x.inputDectectiveBook(realcode);
                    break;
                case 2:
                    while (true) {
                        System.out.print("Nhap code:");
                        sc = new Scanner(System.in);
                        String code = sc.nextLine();
                        realcode = Check.nhapInteger(code);
                        if (realcode >= 0) {
                            break;
                        } else {
                            System.out.println("Code must be integer");
                        }
                    }
                    x.inputScienceBook(realcode);
                    break;
                case 3:
                    System.out.println("Nhap code can xoa :");
                    sc = new Scanner(System.in);
                    String code = sc.nextLine();
                    realcode = Check.nhapInteger(code);
                    x.deleteBook(realcode);
                    break;
                case 4:
                    System.out.print("Nhap code can update:");
                    sc = new Scanner(System.in);
                    code = sc.nextLine();
                    realcode = Integer.parseInt(code);
                    x.updateBook(realcode);
                    break;
                case 5:
                    x.displayDetecticebook();
                    break;
                case 6:
                    x.displayScincebook();
                    break;
            }
        } while (choice != 7);
    }
}
