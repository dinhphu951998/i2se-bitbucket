/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book_manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Dante nguyen
 */
public class bookList {
//    book[]list;
//    int count;
//
//    public bookList() {
//        this.list=new book[100];
//        count=0;
//    }
//    int find(int code){
//        for (int i = 0; i < count; i++) {
//            if(list[i].code==code){
//                return i;
//            }
//        }
//         return -1;
//    }
//    void Add(book x){
//        if(count==list.length){
//            System.out.println("list full");
//        }else{
//        list[count]=x;
//        count++;
//    }
//    }
//    void Delete(int code){
//        Scanner sc=new Scanner(System.in);
//           for (int i = 0; i < count; i++) {
//            if(list[i].code==code){
//                list[i]=list[i+1];
//                count--;
//            }
//        }
//    }
//    void update(int code){
//        Scanner sc=new Scanner(System.in);
//        for (int i = 0; i < count; i++) {
//            if(list[i].code==code){
//                if(list[i] instanceof dectectiveBook){
//                    System.out.print("Nhap price moi:");
//                    int price=sc.nextInt();
//                    System.out.print("Nhap year of publication :");
//                    int newyear=sc.nextInt();
//                    
//                }
//                if(list[i] instanceof scienceBook){
//                    System.out.print("Nhap type :");
//                    String newtype=sc.nextLine();
//                }
//            }    
//        }
//        System.out.println("Updated");
//    }
//    void displayDetectedBook(){
//        int countdecBook=0;
//        for (int i = 0; i < count; i++) {
//           if(list[i] instanceof dectectiveBook){
//               String regex="A-Z+";
//               if(list[i].title.matches(regex)==false){
//                   list[i].title.toLowerCase();
//               }
//               System.out.println(((dectectiveBook)list[i]).toString());
//               countdecBook++;
//               }
//           }
//        int sum=0;
//        for (int i = 0; i < countdecBook; i++) {
//            sum=((dectectiveBook)list[i]).price+sum;
//        }
//        System.out.print("Sum of price: "+sum);
//        }
//    void displayScinceBook(){
//        for (int i = 0; i < count; i++) {
//            if(list[i] instanceof scienceBook){
//                String regex="A-Z+";
//                if(list[i].title.matches(regex)==false){
//                    list[i].title.toLowerCase();
//                }
//                System.out.println(((scienceBook)list[i]).toString());
//            }
//        }
//    }

    ArrayList arr = new ArrayList();

    void addBook(book x) {
        arr.add(x);
    }

    int findBook(int code) {
        for (int i = 0; i < arr.size(); i++) {
            book x = (book) arr.get(i);
            if (x.code == code) {
                return i;
            }
        }
        return -1;
    }

    void inputDectectiveBook(int code) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Nhap price :");
        int price = sc.nextInt();
        System.out.print("Nhap title :");
        sc = new Scanner(System.in);
        String title = sc.nextLine();
        System.out.print("Nhap year :");
        String year = sc.nextLine();
        Date today = new Date(System.currentTimeMillis());
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy");
        String s = timeFormat.format(today.getTime());
        if (year.compareTo(s) <0) {
            dectectiveBook b1 = new dectectiveBook(price, year, code, title);
            addBook(b1);
            System.out.println("Added");
        }else{
            System.out.println("fail to add");
        }
    }

    void inputScienceBook(int realcode) {
        boolean key = false;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap title :");
        String title = sc.nextLine();
        System.out.println("Choose between those type : physical,mathematic,chemistry");
        System.out.print("Nhap type :");
        String type = sc.nextLine();
        String chuoiType = "physical,mathematic,chemistry";
        String[] list = chuoiType.split(",");
        for (int i = 0; i < list.length; i++) {
            if (type.compareTo("mathematic") == 0 || type.compareTo("chemistry") == 0 || type.compareTo("physical") == 0) {
                key = true;
            } else {
                key = false;
            }
        }
        if (key == true) {
            scienceBook b2 = new scienceBook(realcode, title, type);
            addBook(b2);
            System.out.println("Added");

        }else{
            System.out.println("Fail to add");
        }
    }

    void deleteBook(int realcode) {

        int result = findBook(realcode);
        if (result == -1) {
            System.out.println("not found");
        } else {
            book x = (book) arr.remove(result);
            System.out.println("Deleted book " + x.toString());

        }
    }

    void updateBook(int code) {
        int result = findBook(code);
        if (result == -1) {
            System.out.println("not found");
        } else {
            Scanner sc = new Scanner(System.in);
            if (arr.get(result) instanceof dectectiveBook) {
                System.out.print("New price :");
                int price = sc.nextInt();
                System.out.print("new year of publication :");
                sc = new Scanner(System.in);
                String newyear = sc.nextLine();
                System.out.print("new title:");
                sc = new Scanner(System.in);
                String newtitle = sc.nextLine();
                dectectiveBook x1 = new dectectiveBook(price, newyear, code, newtitle);
                arr.set(result, x1);
            }
            if (arr.get(result) instanceof scienceBook) {
                System.out.print("new type :");
                String newtype = sc.nextLine();
                System.out.print("new title :");
                sc = new Scanner(System.in);
                String newtitle2 = sc.nextLine();
                scienceBook x2 = new scienceBook(code, newtitle2, newtype);
                arr.set(result, x2);

            }
        }
    }

    void displayDetecticebook() {

        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i) instanceof dectectiveBook) {
                dectectiveBook x = (dectectiveBook) arr.get(i);
                System.out.println(x.toString());
            }
        }
    }

    void displayScincebook() {
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i) instanceof scienceBook) {
                scienceBook x = (scienceBook) arr.get(i);
                System.out.println(x.toString());
            }
        }
    }
}
