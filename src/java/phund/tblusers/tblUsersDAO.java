/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.tblusers;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phund.utils.DBUtils;

/**
 *
 * @author PhuNDSE63159
 */
public class tblUsersDAO implements Serializable {

    public boolean checkLogin(String user, String pass) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement psm = null;
        ResultSet rs = null;
        //1. Open connection
        try {
            conn = DBUtils.makeConnection();
            if (conn != null) {
                //2. Create sql
                String sql = "select * from tblUsers where username = ? and password = ?";

                //3. Thực hiện truy vấn và truyền tham số -> Tạo PreparedStatement
                psm = conn.prepareStatement(sql);
                psm.setString(1, user);
                psm.setString(2, pass);

                //4. Thực hiện câu lệnh
                rs = psm.executeQuery();
                if (rs.next()) {
                    return true;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;

    }

    private List<tblUsersDTO> list;

    public List<tblUsersDTO> getList() {
        return list;
    }

    public void searchLastName(String txtSearchValue) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement psm = null;
        ResultSet rs = null;
        try {
            conn = DBUtils.makeConnection();
            if (conn != null) {
                String sql = "select * "
                        + "from tblUsers "
                        + "where lastname like ? ";
                psm = conn.prepareStatement(sql);
                psm.setString(1, "%" + txtSearchValue + "%");
                rs = psm.executeQuery();
                if (this.list == null) {
                    this.list = new ArrayList<>();
                }
                while (rs.next()) {
                    String username = rs.getString("username");
                    String password = rs.getString("password");
                    String lastname = rs.getString("lastname");
                    boolean role = rs.getBoolean("isAdmin");
                    tblUsersDTO dto = new tblUsersDTO(username, password, lastname, role);
                    this.list.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

    }

    public boolean deleteAccount(String username) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement psm = null;
        int result;
        try {
            conn = DBUtils.makeConnection();
            if (conn != null) {
                String sql = "delete from tblUsers where username = ?";
                psm = conn.prepareStatement(sql);
                psm.setString(1, username);
                result = psm.executeUpdate();
                if (result > 0) {
                    return true;
                }
            }
        } finally {

            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateAccount(String username, String password, boolean isAdmin) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement psm = null;
        int result;
        try {
            conn = DBUtils.makeConnection();
            if (conn != null) {
                String sql = "update tblUsers set password = ?, isAdmin = ? where username = ?";
                psm = conn.prepareStatement(sql);
                psm.setString(1, password);
                psm.setBoolean(2, isAdmin);
                psm.setString(3, username);
                result = psm.executeUpdate();
                if (result > 0) {
                    return true;
                }
            }
        } finally {
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean createAccount(String username, String password, String fullname, boolean isAdmin) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement psm = null;
        int result = 0;
        //1. Open connection
        try {
            conn = DBUtils.makeConnection();
            if (conn != null) {
                //2. Create sql
                String sql = "insert into tblUsers(username, password, lastname, isAdmin) values(?,?,?,?)";

                //3. Thực hiện truy vấn và truyền tham số -> Tạo PreparedStatement
                psm = conn.prepareStatement(sql);
                psm.setString(1, username);
                psm.setString(2, password);
                psm.setString(3, fullname);
                psm.setBoolean(4, isAdmin);
                //4. Thực hiện câu lệnh
                result = psm.executeUpdate();
                if (result != 0) {
                    return true;
                }
            }
        } finally {
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;

    }
}
