/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phund.struts.update;

import phund.tblusers.tblUsersDAO;
import phund.tblusers.tblUsersDTO;

/**
 *
 * @author PhuNDSE63159
 */
public class UpdateAction extends tblUsersDTO {

    private String searchValue;
    private final String SUCCESS = "success";
    private final String FAIL = "fail";

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public UpdateAction() {
    }

    public String update() throws Exception {
        String url = FAIL;
        tblUsersDAO dao = new tblUsersDAO();
        boolean res = dao.updateAccount(super.getUsername(), super.getPassword(), super.isRole());
        if (res) {
            url = SUCCESS;
        }
        return url;
    }

}
