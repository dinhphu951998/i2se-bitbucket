<%-- 
    Document   : search
    Created on : Jun 28, 2018, 1:47:23 PM
    Author     : PhuNDSE63159
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page</title>
    </head>
    <body>
        <h1>Welcome,
            <s:property value="%{#session.USERNAME}"/>
        </h1>
        <s:form action="search" method="get">
            <s:textfield name="searchValue" label="Search Value"/>
            <s:submit value="Search"/>
        </s:form>
        <s:if test="%{!searchValue.isEmpty()}">
            <s:if test="%{listAccount != null}">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>LastName</th>
                            <th>isAdmin</th>
                            <th>Delete</th>
                            <th>Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        <s:iterator var="dto" value="listAccount" status="counter" >
                            <s:form action="update" theme="simple">
                                <tr>
                                    <td>
                                        <s:property value="#counter.count" />
                                    </td>
                                    <td>
                                        <!--s:property không phải là một control, chỉ là 1 câu lệnh out.println-->
                                        <s:property value="username" />
                                        <s:hidden name="username" value="%{username}"/>
                                    </td>
                                    <td>
                                        <s:textfield value="%{password}" name="password"/>
                                    </td>
                                    <td>
                                        <s:textfield value="%{lastname}" name="lastname"/>
                                    </td>
                                    <td>
                                        <s:checkbox name="role" value="role"/>
                                    </td>
                                    <td>
                                        <s:url action="delete" id="deLink">
                                            <!--Không sử dụng %{} vì s:param không phải là 1 control-->
                                            <!--control là những thứ có thể thao tác được trong page-->
                                            <s:param name="searchValue" value="searchValue"/>
                                            <s:param name="username" value="username"/>
                                        </s:url>
                                        <s:a href="%{deLink}" >Delete</s:a>
                                        </td>
                                        <td>
                                        <s:submit value="Update"/>
                                        <s:hidden name="searchValue" value="%{searchValue}"/>
                                    </td>
                                </tr>
                            </s:form>
                        </s:iterator>
                    </tbody>
                </table>

            </s:if>
            <s:else>
                <h1>There is no record</h1>
            </s:else>
        </s:if>

    </body>
</html>
